﻿using ClubDelLibro.Models.Endidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubDelLibro.Data
{
    public class ClubDelLibroContext : DbContext
    {
        public ClubDelLibroContext(DbContextOptions<ClubDelLibroContext> options) : base(options)
        {
        }

        public DbSet<Libro> Libros { get; set; }
        public DbSet<Socio> Socios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Libro>().ToTable("Libros");
            modelBuilder.Entity<Socio>().ToTable("Socios");
        }

    }
}
