﻿using ClubDelLibro.Models.Endidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClubDelLibro.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ClubDelLibroContext context)
        {
            context.Database.EnsureCreated();

            // Valida si ya hay socios
            if (!context.Socios.Any())
            {
                var socios = new Socio[]
                {
                    new Socio{Nombre="Name 2",Apellido="Apellido 2",Dni=36588692},
                    new Socio{Nombre="Name 3",Apellido="Apellido 3",Dni=36588692},
                    new Socio{Nombre="Name 1",Apellido="Apellido 1",Dni=36588692}
                };
                foreach (Socio c in socios)
                {
                    context.Socios.Add(c);
                }

                context.SaveChanges();
            }

            if (!context.Libros.Any())
            {
                var libros = new Libro[]
                {
                    new Libro{Titulo="Libro 1",ISBN=123456,Anio=2005,DuenioFK = 1},
                    new Libro{Titulo="Libro 2",ISBN=123456,Anio=2002,DuenioFK = 1},
                    new Libro{Titulo="Libro 3",ISBN=123456,Anio=2003,DuenioFK = 1},
                    new Libro{Titulo="Libro 4",ISBN=123456,Anio=2002,DuenioFK = 2},
                    new Libro{Titulo="Libro 5",ISBN=123456,Anio=2002,DuenioFK = 2},
                    new Libro{Titulo="Libro 6",ISBN=123456,Anio=2001,DuenioFK = 2},
                    new Libro{Titulo="Libro 7",ISBN=123456,Anio=2003,DuenioFK = 3},
                    new Libro{Titulo="Libro 8",ISBN=123456,Anio=2005,DuenioFK = 3}
                };
                foreach (Libro s in libros)
                {
                    context.Libros.Add(s);
                }

                context.SaveChanges();
            }

            
        }
    }
}