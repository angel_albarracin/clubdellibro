﻿using System.Threading.Tasks;
using ClubDelLibro.Data;
using ClubDelLibro.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClubDelLibro.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PrestamosController : ControllerBase
    {
        private readonly ClubDelLibroContext _context;

        public PrestamosController(ClubDelLibroContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> PostSocio([FromBody] Prestamo prestamo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var libro = await _context.Libros.FindAsync(prestamo.LibroId);
            var socio = await _context.Socios.FindAsync(prestamo.SocioId);

            libro.PrestadoA = socio;
            _context.Entry(libro).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return Ok(await _context.Socios.FindAsync(libro.ID));
        }

    }

}