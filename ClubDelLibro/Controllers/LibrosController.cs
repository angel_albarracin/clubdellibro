﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClubDelLibro.Data;
using ClubDelLibro.Models.Endidades;
using Microsoft.AspNetCore.Authorization;
using ClubDelLibro.Models;

namespace ClubDelLibro.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        private readonly ClubDelLibroContext _context;

        public LibrosController(ClubDelLibroContext context)
        {
            _context = context;
        }

        // GET: api/Libros
        [HttpGet]
        public IEnumerable<Libro> GetLibros([FromQuery] bool? prestado = null )
        {
            List<Libro> libros = new List<Libro>();
            if (prestado.HasValue)
                libros = _context.Libros.Where(x => (x.PrestadoA != null && prestado.Value) || (x.PrestadoA == null && !prestado.Value)).ToList();
            else
                libros = _context.Libros.ToList();
            return libros;
        }

        // GET: api/Libros/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLibro([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var libro = await _context.Libros.FindAsync(id);

            if (libro == null)
            {
                return NotFound();
            }

            return Ok(libro);
        }

        // PUT: api/Libros/5/Devolver
        [HttpPut("{id}/Devolver")]
        public async Task<IActionResult> DevolverLibro([FromRoute] int id, [FromBody] Prestamo prestamo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != prestamo.LibroId)
                return BadRequest();

            var libro = _context.Libros.Find(prestamo.LibroId);

            if (libro.PrestadoAFK != prestamo.SocioId)
                return BadRequest();

            libro.PrestadoAFK = null;
            _context.Entry(libro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Libros.Any(x => x.ID == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_context.Libros.Find(prestamo.LibroId));
        }

    }
}