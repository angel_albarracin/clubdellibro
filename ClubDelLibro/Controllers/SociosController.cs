﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ClubDelLibro.Data;
using ClubDelLibro.Models.Endidades;
using Microsoft.AspNetCore.Authorization;

namespace ClubDelLibro.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SociosController : ControllerBase
    {
        private readonly ClubDelLibroContext _context;

        public SociosController(ClubDelLibroContext context)
        {
            _context = context;
        }

        [HttpGet("{id}/Prestamos")]
        public IEnumerable<Libro> GetPrestamos([FromRoute] int id)
        {
            return _context.Libros.Where(x => x.PrestadoAFK == id).ToList();
        }

    }
}