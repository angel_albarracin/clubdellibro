﻿using System;
using System.IO;

namespace ClubDelLibro.Models
{
    public class Log
    {

        public static void Logear(string text)
        {
            using (StreamWriter textWriter = File.AppendText("log.txt"))
            {
                Escribir(text, textWriter);
                Console.Write(text, Environment.NewLine);
            }
        }

        private static void Escribir(string text, TextWriter textWriter)
        {
            textWriter.WriteLine("-------------------------------");
            textWriter.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            textWriter.WriteLine($"Log Entry : {text}");
            textWriter.WriteLine("-------------------------------");
        }
    }
}
