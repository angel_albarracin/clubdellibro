﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClubDelLibro.Models.Endidades
{
    public class Libro
    {
        public int ID { get; set; }
        public string Titulo { get; set; }
        public long ISBN { get; set; }
        public string Autor { get; set; }
        public int Anio { get; set; }
        public string Editorial { get; set; }

        public int DuenioFK { get; set; }
        [ForeignKey("DuenioFK")]
        public Socio Duenio { get; set; }

        public int? PrestadoAFK { get; set; }
        [ForeignKey("PrestadoAFK")]
        public Socio PrestadoA { get; set; }
    }
}
