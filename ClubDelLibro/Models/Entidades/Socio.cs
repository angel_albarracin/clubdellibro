﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubDelLibro.Models.Endidades
{
    public class Socio
    {
        public static int MAXPRESTAMOS { get { return 5; } }
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Dni { get; set; }
        public string Email { get; set; }
    }
}
