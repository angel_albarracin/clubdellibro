﻿namespace ClubDelLibro.Models
{
    public class Prestamo
    {
        public int SocioId { get; set; }
        public int LibroId { get; set; }
    }
}
